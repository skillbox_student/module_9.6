terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}


# Указываем, что мы хотим разворачивать окружение в Yandex Cloud
provider "yandex" {
  service_account_key_file = "key.json"
}

data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-2004-lts"
}

resource "yandex_lb_network_load_balancer" "web" {
  name      = "web-network-load-balancer"
  folder_id = "b1gkfd8oio27g9cmdcs8"

  listener {
    name = "my-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web" {
  name      = "web-target-group"
  folder_id = "b1gkfd8oio27g9cmdcs8"


  target {
    subnet_id = "e9btd31trknk7ofi07s3"
    address   = yandex_compute_instance_group.web.instances[0].network_interface.0.ip_address
  }

  target {
    subnet_id = "e9btd31trknk7ofi07s3"
    address   = yandex_compute_instance_group.web.instances[1].network_interface.0.ip_address
  }
}

resource "yandex_compute_instance_group" "web" {
  name                = "web-ig"
  folder_id           = "b1gkfd8oio27g9cmdcs8"
  service_account_id  = "ajelu9ls6bc50mvf4dl8"
  deletion_protection = false
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory = 2
      cores  = 2
      # Для экономии средств - гарантированная доля vCPU 20%
      core_fraction = 20
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = data.yandex_compute_image.ubuntu.id
        size     = 5
      }
    }
    network_interface {
      network_id = "enp70tm672nikk8rjqo5"
      subnet_ids = ["e9btd31trknk7ofi07s3"]
      nat        = true
    }
    metadata = {
      ssh-keys  = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      user_data = file("user_data.sh")
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
}

resource "yandex_compute_instance" "my_webserver" {
  zone        = "ru-central1-a"
  platform_id = "standard-v1"
  folder_id   = "b1gkfd8oio27g9cmdcs8"

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.id
      size     = 5
    }
  }
  network_interface {
    subnet_id = "e9btd31trknk7ofi07s3"
    nat       = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
  resources {
    cores  = 2
    memory = 2
    # Для экономии средств - гарантированная доля vCPU 20%
    core_fraction = 20
  }
}

output "nginx_1_public_ip_address" {
  value = yandex_compute_instance_group.web.instances[0].network_interface.0.nat_ip_address
}

output "nginx_2_public_ip_address" {
  value = yandex_compute_instance_group.web.instances[1].network_interface.0.nat_ip_address
}

output "load_balancer_public_ip_address" {
  value = yandex_lb_network_load_balancer.web.listener
}

output "reactjs_1_public_ip_address" {
  value = yandex_compute_instance.my_webserver.network_interface.0.nat_ip_address
}